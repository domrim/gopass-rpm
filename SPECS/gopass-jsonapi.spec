%define debug_package %{nil}
%define repo github.com/gopasspw/gopass-jsonapi

Name:           gopass-jsonapi
Version:        1.14.6
Release:        1%{?dist}
Summary:        The slightly more awesome standard unix password manager for teams

Group:          Applications/System
License:        MIT
URL:            https://%{repo}
Source0:        https://%{repo}/releases/download/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  tar gzip git golang
Requires:       gopass

%description
JSON-API module for gopass

%prep
%setup -q -c -n %{name}-%{version}
cd ..
mkdir -p $(dirname src/%{repo})
mv %{name}-%{version} src/%{repo}
mkdir %{name}-%{version}
mv src %{name}-%{version}

%build
export GOPATH="$(pwd)"
export PATH=$PATH:"$(pwd)"/bin
cd src/%{repo}
make build
make completion

%install
install -D src/%{repo}/gopass-jsonapi %{buildroot}%{_bindir}/gopass-jsonapi

%files
%{_bindir}/gopass-jsonapi
%license src/%{repo}/LICENSE

%changelog
* Wed Sep 14 2022 Dominik Rimpf <dev@drimpf.de> - 1.14.6-1
- Bump version

* Sat May 01 2021 Dominik Rimpf <dev@drimpf.de> - 1.11.1-1
- Initial RPM Build 1.11.1

